# graph_intervals

## How to test

```
>$go mod tidy
>$go test ./...
```
In order to test target function, just run commands above.

## How to bench []*Transaction vs []Transaction

```
>$go test -bench=. -count 10 > run.txt 
>$benchstat run.txt
```
In order to benchmark using different types.
