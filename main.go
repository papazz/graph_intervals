package graph_intervals

import	"time"

const (
	secPerMinute int64 = 60
	secPerHour         = 60 * secPerMinute
	secPerDay          = 24 * secPerHour
	secPerWeek         = 7 * secPerDay
	/*
	 * Let#s say avg. in month 30 days
	 */
	secPerMonth = 30 * secPerWeek
)

type Transaction struct {
	Value     int
	Timestamp time.Time
}

func distribution(transactions []Transaction, interval string) []Transaction {
	if len(transactions) < 1 {
		return nil
	}
	m := make(map[int64]Transaction)
	var secondsIn = func(interval string) int64 {
		switch interval {
		case "hour":
			return secPerHour
		case "day":
			return secPerDay
		case "week":
			return secPerWeek
		case "month":
			return secPerMonth
		default:
			return secPerHour
		}
	}
	/*
	 * Grouping function
	 */
	var mapFunc = func(tx Transaction, interval string) {
		txUnix := tx.Timestamp.Unix()
		key := txUnix - (txUnix % secondsIn(interval))
		if v, ok := m[key]; ok {
			if txUnix > v.Timestamp.Unix() {
				m[key] = tx
				return
			}
			return
		}
		m[key] = tx
	}
	/*
	 * Process transactions
	 */
	for idx := range transactions {
		mapFunc(transactions[idx], interval)
	}
	return func() []Transaction {
		var response []Transaction
		for k, v := range m {
			response = append(response, Transaction{
				Value:     v.Value,
				Timestamp: time.Unix(k, 0),
			})
		}
		return response
	}()
}
