package graph_intervals

import (
	"reflect"
	"testing"
	"time"
)

func Test_distribution(t *testing.T) {
	var eq = func(got, want []Transaction) bool {
		eqCtr := 0
		for _, a := range got {
			for _, b := range want {
				if reflect.DeepEqual(a, b) {
					eqCtr++
				}
			}
		}
		if eqCtr != len(want) || len(got) != len(want) {
			return false
		}
		return true
	}
	type args struct {
		transactions []Transaction
		interval     string
	}
	tests := []struct {
		name string
		args args
		want []Transaction
	}{
		{
			name: "hour_test",
			args: args{
				transactions: []Transaction{
					{
						Value:     4456,
						Timestamp: time.Unix(1616026248, 0),
					},
					{
						Value:     4231,
						Timestamp: time.Unix(1616022648, 0),
					},
					{
						Value:     5212,
						Timestamp: time.Unix(1616019048, 0),
					},
					{
						Value:     4321,
						Timestamp: time.Unix(1615889448, 0),
					},
					{
						Value:     4567,
						Timestamp: time.Unix(1615871448, 0),
					},
				},
				interval: "hour",
			},
			want: []Transaction{
				{
					Value:     4456,
					Timestamp: time.Unix(1616025600, 0),
				},
				{
					Value:     4231,
					Timestamp: time.Unix(1616022000, 0),
				},
				{
					Value:     4321,
					Timestamp: time.Unix(1615888800, 0),
				},
				{
					Value:     5212,
					Timestamp: time.Unix(1616018400, 0),
				},
				{
					Value:     4567,
					Timestamp: time.Unix(1615870800, 0),
				},
			},
		},
		{
			name: "day_test",
			args: args{
				transactions: []Transaction{
					{
						Value:     4456,
						Timestamp: time.Unix(1616026248, 0),
					},
					{
						Value:     4231,
						Timestamp: time.Unix(1616022648, 0),
					},
					{
						Value:     5212,
						Timestamp: time.Unix(1616019048, 0),
					},
					{
						Value:     4321,
						Timestamp: time.Unix(1615889448, 0),
					},
					{
						Value:     4567,
						Timestamp: time.Unix(1615871448, 0),
					},
				},
				interval: "day",
			},
			want: []Transaction{
				{
					Value:     4456,
					Timestamp: time.Unix(1616025600, 0),
				},
				{
					Value:     4231,
					Timestamp: time.Unix(1615939200, 0),
				},
				{
					Value:     4321,
					Timestamp: time.Unix(1615852800, 0),
				},
			},
		},
		{
			name: "week_test",
			args: args{
				transactions: []Transaction{
					{
						Value:     4456,
						Timestamp: time.Unix(1616026248, 0),
					},
					{
						Value:     4231,
						Timestamp: time.Unix(1616022648, 0),
					},
					{
						Value:     5212,
						Timestamp: time.Unix(1616019048, 0),
					},
					{
						Value:     4321,
						Timestamp: time.Unix(1615889448, 0),
					},
					{
						Value:     4567,
						Timestamp: time.Unix(1615871448, 0),
					},
				},
				interval: "week",
			},
			want: []Transaction{
				{
					Value:     4456,
					Timestamp: time.Unix(1616025600, 0),
				},
				{
					Value:     4231,
					Timestamp: time.Unix(1615420800, 0),
				},
			},
		},
		{
			name: "month_test",
			args: args{
				transactions: []Transaction{
					{
						Value:     4456,
						Timestamp: time.Unix(1616026248, 0),
					},
					{
						Value:     4231,
						Timestamp: time.Unix(1616022648, 0),
					},
					{
						Value:     5212,
						Timestamp: time.Unix(1616019048, 0),
					},
					{
						Value:     4321,
						Timestamp: time.Unix(1615889448, 0),
					},
					{
						Value:     4567,
						Timestamp: time.Unix(1615871448, 0),
					},
				},
				interval: "month",
			},
			want: []Transaction{
				{
					Value:     4456,
					Timestamp: time.Unix(1614816000, 0),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := distribution(tt.args.transactions, tt.args.interval); !eq(got, tt.want) {
				t.Errorf("distribution() = %v, want %v", got, tt.want)
			}
		})
	}
}
