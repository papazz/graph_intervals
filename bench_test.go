package graph_intervals

import (
	"testing"
	"time"
)

func BenchmarkSlicePointers(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		slice := make([]*Transaction, 0, 100)
		for j := 0; j < 100; j++ {
			slice = append(slice, &Transaction{Value: j, Timestamp: time.Unix(time.Now().Unix() + int64(j), 0)})
		}
	}
}

func BenchmarkSliceWithoutPointer(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		slice := make([]Transaction, 0, 100)
		for j := 0; j < 100; j++ {
			slice = append(slice, Transaction{Value: j, Timestamp: time.Unix(time.Now().Unix() + int64(j), 0)})
		}
	}
}
